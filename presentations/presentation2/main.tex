\documentclass[
12pt,notheorems,hyperref={pdfauthor=whatever},spanish
]{beamer}

\input{loadslides.tex} % Loads packages and some defined commands

\title[
% Text entered here will appear in the bottom middle
]{Control de Acceso y Autenticación}

\subtitle{Seguridad en Sistemas Operativos}

\author[
% Text entered here will appear in the bottom left corner
]{
    Nicolás Andrés Ramírez Calderón\\
    Víctor Yotecatl González Jiménez\\
    Javier de Jesús Infante Martínez
}
\institute{
    Sistemas Operativos\\
    Ingeniería en Computación\\
    Universidad Nacional Autónoma de México}
\date{Mayo 13, 2024}

\begin{document}

% Generate title page
{
\setbeamertemplate{footline}{} 
\begin{frame}
  \titlepage
\end{frame}
}
\addtocounter{framenumber}{-1}

% You can declare different parts as a parentof sections
\begin{frame}{Parte I: Control de acceso a los recursos}
    \tableofcontents[part=1]
\end{frame}
\begin{frame}{Parte II: Autenticación}
    \tableofcontents[part=2]
\end{frame}
\begin{frame}{Parte III: Seguridad en GNU/Linux}
    \tableofcontents[part=3]
\end{frame}

\makepart{Control de acceso a los recursos}
\begin{frame}
    \large
    \begin{figure}
        \centering
        \includegraphics[width=4cm]{images/acess-control.png}
        \\
        \Large
        \textit{"Modelos claros, protección eficaz"}
    \end{figure}
    \vspace{1cm}
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \textbf{¿Qué se quiere proteger?} & \hspace{3cm} & \textbf{¿Quién puede hacer qué?}
        \end{tabular}
    \end{table}
\end{frame}
\section{Dominios de protección}
\begin{frame}
    \large
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=3.5cm]{images/object.png} & \hspace{1cm} &\includegraphics[width=4cm]{images/operations.png}\\
        \end{tabular}
        \\
        \vspace{0.5cm}
        \textbf{Objeto y Permisos}
        \vspace{1cm}
    \end{table}
    \begin{itemize}
        \item Cada objeto tiene un nombre único referenciable.
        \item Los procesos tienen un conjunto bien definido de operaciones sobre los objetos.
    \end{itemize}
\end{frame}

\subsection{Principle of Least Authority}
    \begin{frame}[fragile]
        \centering
        \Huge
        \textbf{POLA}\\
        \normalsize
        \textit{"Principle of Least Authority"}
        \vspace{1cm}
        \begin{table}
            \centering
            \Large
            \begin{tabular}{ccc}
                \textbf{\textcolor{red}{- Privilegios}} & \hspace{2cm} & \textbf{\textcolor{green}{+ Seguridad}}
            \end{tabular}
            \\
            \large
            \vspace{1cm}
            La seguridad funciona mejor cuando cada dominio tiene los\\ mínimos objetos y privilegios para realizar su trabajo; y no más.
        \end{table}
    \end{frame}
\subsection{Implementación en UNIX}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=17cm]{images/protection-matrix.jpg}
    \end{figure}
\end{frame}
\begin{frame}
    \large
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=3cm]{images/domain.png} & \multirow{3}{*}{\hspace{1cm}} & \includegraphics[width=3cm]{images/process.png}\\
            \textbf{Dominio} & & \textbf{Proceso} \\
            Definido mediante UID y GID & & Por parte de usuario y parte de kernel \\
        \end{tabular}
        \vspace{0.5cm}
        \vspace{1cm}
    \end{table}
    \begin{itemize}
        \item \textbf{UID:} User ID
        \item \textbf{GID:} Group ID
    \end{itemize}
\end{frame}
\section{Listas de control de acceso}
\begin{frame}
    \large
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=4cm]{images/object.png} &  \hspace{1cm} & \includegraphics[width=4cm]{images/list.png}
        \end{tabular}
    \end{table}
    \vspace{1cm}
    \begin{itemize}
        \item Se asocia cada objeto con una \textbf{\color{blue}{lista ordenada}}.
        \item La \textbf{\color{blue}{lista ordenada}} contiene los dominios que acceden al objeto y la forma de hacerlo.
    \end{itemize}
\end{frame}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=17cm]{images/acess-lists.jpg}
    \end{figure}
\end{frame}
\section{Capacidades}
\begin{frame}
    \large
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=4cm]{images/process.png} &  \hspace{1cm} & \includegraphics[width=4cm]{images/list.png}
        \end{tabular}
    \end{table}
    \vspace{1cm}
    \begin{itemize}
        \item A cada proceso se le asocia una \textbf{\color{blue}{lista de objetos}} que puede utilizar con sus respectivas \textbf{\color{green}{operaciones permitidas}} en cada objeto.
        \item Ésta es la \textbf{Lista-C}, cada elemento de ella es una \textbf{capacidad}. 
    \end{itemize}
\end{frame}
\subsection{Protección de la lista de capacidades}
\begin{frame}
    \begin{table}
        \centering
        \Large
        \textbf{Métodos de protección}\\
        \hline
        \hline
        \large
        \renewcommand{\arraystretch}{3}
        \begin{tabular}{ccccc}
            \multirow{2}{*}{\includegraphics[width=3cm]{images/tag.png}} & \multirow{3}{*}{\hspace{0.5cm}} & {\textbf{Dentro del SO}} & \multirow{3}{*}{\hspace{0.5cm}} & \multirow{2}{*}{\includegraphics[width=3cm]{images/cipher.png}} \\
            & & \multirow{2}{*}{\includegraphics[width=3cm]{images/inside.png}} & & \\
            \renewcommand{\arraystretch}{1}
            \textbf{\begin{tabular}{c}
                Arquitectura\\
                etiquetada
            \end{tabular}}
            \renewcommand{\arraystretch}{3}
            & & & & \textbf{Cifrada}
            \\
            \hline
            \renewcommand{\arraystretch}{1}
            \begin{tabular}{c}
            \normalsize
                Bit adicional
            \end{tabular}  & & & & 
            \renewcommand{\arraystretch}{1}
            \begin{tabular}{c}
            \normalsize
                Encriptado en el\\
            \normalsize
                espacio de usuario
            \end{tabular} 
        \end{tabular}
    \end{table}
\end{frame}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=17cm]{images/encrypted-capability.jpg}
    \end{figure}
    \vspace{1cm}
    \begin{itemize}
        \large
        \item El servidor genera y devuelve una capacidad al usuario.
    \end{itemize}
\end{frame}
\makepart{Autenticación}
\begin{frame}
    \begin{table}
        \centering
        \Large
        \textbf{Tipos de autenticación}\\
        \vspace{1cm}
        \hline
        \hline
        \large
        \renewcommand{\arraystretch}{3}
        \begin{tabular}{ccccc}
            \multirow{2}{*}{\includegraphics[width=3cm]{images/though.png}} & \multirow{3}{*}{\hspace{0.5cm}} & {\textbf{Tiene}} & \multirow{3}{*}{\hspace{0.5cm}} & \multirow{2}{*}{\includegraphics[width=3cm]{images/id.png}} \\
            & & \multirow{2}{*}{\includegraphics[width=3cm]{images/keys.png}} & & \\
            \textbf{Conoce}
            & & & & \textbf{Es}
        \end{tabular}
    \end{table}
\end{frame}
\section{Contraseñas}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=17cm]{images/contraseña.jpg}
    \end{figure}
    \vspace{1cm}
    \begin{table}
        \centering
        \begin{tabular}{lcc}
            \tabitem Un \textbf{usuario} relacionado a una \textbf{contraseña}. & \multirow{3}{*}{\hspace{0.5cm}} & \multirow{3}{*}{\includegraphics[width=2.5cm]{images/password.png}}  \\
            \tabitem \textbf{Windows}: Esconde los caracteres con \textbf{*}. & & \\
            \tabitem \textbf{UNIX}: No muestra nada. & &
        \end{tabular}
    \end{table}
\end{frame}
\subsection{Vulnerabilidades}
\begin{frame}
    \large
    \begin{table}
        \centering
        \begin{tabular}{ccc}
            \includegraphics[width=3cm]{images/dictionary.png} & \multirow{3}{*}{\hspace{1cm}} & \includegraphics[width=3cm]{images/dialer.png}\\
            \textbf{Fuerza bruta} & & \textbf{War Dialer} \\
            Con diccionario & & En busca de sincronización\\
        \end{tabular}
    \end{table}
\end{frame}
\subsection{Automatización}
\begin{frame}
    \large
    \begin{figure}
        \centering
        \includegraphics[width=4cm]{images/dice.png}
        \\
        \Large
        \textit{Generación Aleatoria}
    \end{figure}
    \vspace{1cm}
    \begin{itemize}
        \item\textbf{Usuarios}
        \item\textbf{Contraseñas}
    \end{itemize}
\end{frame}
\subsection{Métodos de seguridad}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=4cm]{images/cipher.png}
    \end{figure}
    \begin{table}
        \centering
    \begin{itemize}
        \large
        \item \textbf{Cifrado}
        \item \textbf{SALT}
        \item \textbf{Medidas extra}
    \end{itemize}
    \end{table}
\end{frame}
\subsection{Contraseñas de un solo uso}
\begin{frame}
    \large
    \begin{figure}
        \centering
        \includegraphics[width=4cm]{images/otp.png}
        \\
        \Large
        \textit{"One Time Passwords"}
    \end{figure}
    \vspace{1cm}
    \textbf{OTP:}
    \begin{itemize}
        \item Acceder de forma \textbf{\color{green}{segura}} en una red \textbf{\color{red}{insegura}}
        \item El filtrado de la contraseña no tiene un impacto significativo.
    \end{itemize}
\end{frame}
\section{Objeto físico}
\begin{frame}[fragile]
    \begin{figure}
        \centering
        \includegraphics[width=4cm]{images/cards.png}
    \end{figure}
    \vspace{0.5cm}
    \begin{table}
        \centering
        \Large
        \begin{tabular}{ccc}
            \hspace{1cm} \textbf{\textcolor{red}{\underline{Precio}}} & \hspace{1cm} & \textbf{\textcolor{green}{\underline{Seguridad}}}
        \end{tabular}
        \\
        \large
        \vspace{1cm}
        \begin{itemize}
            \item De cinta magnética y de chip.
        \end{itemize}
    \end{table}
\end{frame}
\section{Biometría}
\begin{frame}
    \begin{table}
        \centering
        \Large
        \textbf{Tipos de biometría}\\
        \vspace{1cm}
        \hline
        \hline
        \large
        \renewcommand{\arraystretch}{3}
        \begin{tabular}{ccccc}
            \multirow{2}{*}{\includegraphics[width=3cm]{images/id.png}} & \multirow{3}{*}{\hspace{0.5cm}} & {\textbf{Facial}} & \multirow{3}{*}{\hspace{0.5cm}} & \multirow{2}{*}{\includegraphics[width=3cm]{images/voice-recognition.png}} \\
            & & \multirow{2}{*}{\includegraphics[width=3cm]{images/face-recognition.png}} & & \\
            \textbf{Dactilar}
            & & & & \textbf{Por voz}
        \end{tabular}
    \end{table}
    \begin{itemize}
        \large
        \item Suelen ser alternativas más \textbf{\color{green}{seguras}}.
    \end{itemize}
\end{frame}
\makepart{Seguridad en GNU/Linux}
\begin{frame}
    \begin{figure}[!ht]
        \centering
        \includegraphics[width=8cm]{images/unix.png}
        \\
        \hline
        \vspace{1cm}
        \textbf{Hijos de UNIX}
    \end{figure}
    \begin{itemize}
        \item Sistema \textbf{multiusuario} desde sus inicios
        \item De la mano con \textbf{seguridad} y \textbf{control de la información}.

    \end{itemize}
\end{frame}
\section{Conceptos fundamentales}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=6cm]{images/chmod777.png}
    \end{figure}
    \vspace{1cm}
    \begin{table}
        \begin{tabular}{lcc}
            \tabitem \textbf{UID}: Usuario. Para archivos, procesos y otros... & \multirow{5}{*}{\hspace{1cm}} & \multirow{5}{*}{
                \includegraphics[width=3cm]{images/admin.png}
            } \\
            \tabitem \textbf{GID}: Grupo. Organización de usuarios & & \\
            \tabitem \textbf{Permisos}: \textit{read}, \textit{write} y \textit{execute}. & & \\
            \tabitem \textbf{Root}: o Superusuario. & & \\
            \tabitem \textbf{SETUID bit}: Nuevo poder de UID. & & \\
        \end{tabular}
    \end{table}
\end{frame}
\subsection{Bits de protección}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=17cm]{images/file-protection-modes.jpg}
    \end{figure}
\end{frame}
\section{Seguridad en las llamadas al sistema}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=5cm]{images/system-calls.jpg}
        \\
        \textbf{System Calls}
    \end{figure}
    \vspace{0.4cm}
    \begin{table}
        \emphasize{\textbf{¿En qué consisten?}}
        \\
        \vspace{0.4cm}
        \centering
        \begin{tabular}{ccccc}
            \includegraphics[width=2cm]{images/modify.png} & \multirow{2}{*}{\hspace{2cm}} & \includegraphics[width=2cm]{images/id.png} & \multirow{2}{*}{\hspace{2cm}} & \includegraphics[width=3.5cm]{images/permissions.png}\\
            \textbf{Obtener y Modificar} & & \textbf{UID, GID} & & \textbf{y Permisos}
        \end{tabular}
    \end{table}
\end{frame}
\subsection{Ejemplos}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=15cm]{images/system-calls-ex.jpg}
    \end{figure}
\end{frame}
\section{Implementaciones de seguridad}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=5cm]{images/security.png}
    \end{figure}
    \begin{table}
        \centering
        \Large
        \begin{tabular}{ccccccc}
            \textbf{\textcolor{red}{Hashing}} & \hspace{0.75cm} & \textbf{\textcolor{pink}{Asignación}} & \hspace{0.75cm} & \textbf{\textcolor{green}{Herencia}} & \hspace{0.75cm} & \textbf{\textcolor{blue}{Verificación}}
        \end{tabular}
    \end{table}
\end{frame}
\makepart{Bibliografía}
\section{Modern Operating Systems}
\begin{frame}
    \begin{figure}
        \centering
        \includegraphics[width=6cm]{images/mos.jpg}
        \\
        \Large
        \textbf{Tanenbaum, A. \ \ \ Bos, H.}
    \end{figure}
\end{frame}
\end{document}
