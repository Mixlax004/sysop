import java.util.concurrent.Semaphore;

public class ModifiedSemaphoreTest {

    static Semaphore semaphore = new Semaphore(4);

    static class ModifiedPruebaHilos extends Thread {

        String name = "";

        ModifiedPruebaHilos(String name) {
            this.name = name;
        }

        public void run() {
            try {
                System.out.println(name + " trying to acquire permit...");

                // Attempt to enter the critical section
                semaphore.acquire();
                System.out.println(name + " successfully entered critical section!");

                try {

                    for (int i = 1; i <= 5; i++) {

                        System.out.println(name + " performing iteration " + i);

                        Thread.sleep(1000);
                        System.out.println(name + " completed iteration " + i);

                    }

                } finally {

                    // Leave the critical section
                    System.out.println(name + " leaving critical section...");
                    semaphore.release();
                    System.out.println(name + "'s permit returned. Available permits: " 
                            + semaphore.availablePermits());
                }

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

    }

    public static void main(String[] args) {

        System.out.println("Initial available permits: " + semaphore.availablePermits());

        ModifiedPruebaHilos t1 = new ModifiedPruebaHilos("A");
        t1.start();

        ModifiedPruebaHilos t2 = new ModifiedPruebaHilos("B");
        t2.start();
    }
}
