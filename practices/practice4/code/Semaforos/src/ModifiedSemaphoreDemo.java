import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;

// Shared resource/class
class Shared {
    static volatile int count = 0; // Accessing 'volatile' keyword ensures visibility across threads
}

// Extends Runnable rather than Thread for greater flexibility
class MyRunnable implements Runnable {
    private Semaphore sem;
    private final String threadName;

    public MyRunnable(Semaphore sem, String threadName) {
        this.sem = sem;
        this.threadName = threadName;
    }

    @Override
    public void run() {
        try {
            System.out.println("Attempting to acquire permit for " + threadName);
            boolean permitAcquired = false;
            try {
                // Improvement: Use tryAcquire() with timeout to detect stuck threads
                permitAcquired = sem.tryAcquire(1, TimeUnit.SECONDS);
            } catch (InterruptedException ignored) {}

            if (!permitAcquired) {
                throw new RuntimeException("Could not acquire permit!");
            }

            System.out.println(threadName + " acquired permit.");

            // Critical section begins
            for (int i = 0; i < 5; i++) {
                if (threadName == "A") {
                    Shared.count++;
                    System.out.println(threadName + ": " + Shared.count);
                } else {
                    Shared.count--;
                    System.out.println(threadName + ": " + Shared.count);
                }

                // Allow brief pauses promoting fairness
                TimeUnit.MILLISECONDS.sleep(100);
            }
            // Critical section ends

            System.out.println(threadName + " releasing permit.");
            sem.release();
        } catch (InterruptedException e) {
            System.err.println(e);
        }
    }
}

// Main class
public class ModifiedSemaphoreDemo {
    public static void main(String[] args) throws InterruptedException {
        // Create a binary semaphore (with only 1 permit allowed)
        Semaphore semaphore = new Semaphore(1);

        // Create two Runnables with names A and B
        MyRunnable taskA = new MyRunnable(semaphore, "A");
        MyRunnable taskB = new MyRunnable(semaphore, "B");

        // Start threads A and B
        Thread tA = new Thread(taskA);
        Thread tB = new Thread(taskB);

        tA.start();
        tB.start();

        // Wait for threads A and B to terminate
        tA.join();
        tB.join();

        // Final check to verify shared variable 'count' equals 0
        System.out.println("Final count: " + Shared.count);
    }
}
