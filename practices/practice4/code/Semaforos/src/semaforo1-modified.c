#include <stdio.h>
#include <pthread.h>
#include <semaphore.h>
#define SHARED 1

void *Producer();
void *Consumer();
sem_t empty, full, sm;
int data;

int main() {
    printf("Main started.\n");
    sem_init(&empty, SHARED, 1);
    sem_init(&full, SHARED, 0);
    sem_init(&sm, SHARED, 1);
    
    pthread_t ptid, ctid;
    pthread_create(&ptid, NULL, Producer, NULL);
    pthread_create(&ctid, NULL, Consumer, NULL);

    pthread_join(ptid, NULL);
    pthread_join(ctid, NULL);

    printf("Main finished.\n");
}

void *Producer() {
    int produced;
    printf("Producer created.\n");
    printf("Producer ID: %lu\n", pthread_self());

    for (produced = 0; produced < 5; produced++) {
        printf("Producer waiting for empty semaphore (%d)...\n", produced);
        sem_wait(&empty);
        printf("Producer acquired empty semaphore (%d).\n", produced);
        
        printf("Producer waiting for sm semaphore (%d)...\n", produced);
        sem_wait(&sm);
        printf("Producer acquired sm semaphore (%d).\n", produced);
        
        data = produced;
        printf("Producer produced (%d)\n", data);
        
        printf("Producer releasing sm semaphore (%d)...\n", produced);
        sem_post(&sm);
        printf("Producer released sm semaphore (%d).\n", produced);
        
        printf("Producer signaling full (+%d)...\n", produced + 1);
        sem_post(&full);
        printf("Producer signaled full (+%d).\n", produced + 1);
    }
}

void *Consumer() {
    int consumed, total = 0;
    printf("Consumer created.\n");
    printf("Consumer ID: %lu\n", pthread_self());

    for (consumed = 0; consumed < 5; consumed++) {
        printf("Consumer waiting for full semaphore (%d)...\n", consumed);
        sem_wait(&full);
        printf("Consumer acquired full semaphore (%d).\n", consumed);
        
        printf("Consumer waiting for sm semaphore (%d)...\n", consumed);
        sem_wait(&sm);
        printf("Consumer acquired sm semaphore (%d).\n", consumed);
        
        total += data;
        printf("Consumer consumed (%d), Total=%d\n", data, total);
        
        printf("Consumer releasing sm semaphore (%d)...\n", consumed);
        sem_post(&sm);
        printf("Consumer released sm semaphore (%d).\n", consumed);
        
        printf("Consumer signaling empty (-%d)...\n", consumed + 1);
        sem_post(&empty);
        printf("Consumer signaled empty (-%d).\n", consumed + 1);
    }

    printf("Total sum of 5 iterations is %d\n", total);
}
