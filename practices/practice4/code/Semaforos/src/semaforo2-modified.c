#include <pthread.h>
#include <semaphore.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/syscall.h> /*For gettid*/
#include <unistd.h>       /*For sysconf*/

#define MaxItems 5
#define BufferSize 5

sem_t empty;
sem_t full;
int in = 0;
int out = 0;
int buffer[BufferSize];
pthread_mutex_t mutex;

void *producer(void *arg)
{
    long tid = syscall(__NR_gettid);
    int item;
    sleep(1); // Sleep to illustrate timing differences clearly
    for (int i = 0; i < MaxItems; ++i)
    {
        item = rand();
        fprintf(stderr, "[PRODUCER-%ld] Going to WAIT for EMPTY condition...\n", tid);
        sem_wait(&empty);

        fprintf(stderr, "[PRODUCER-%ld] Acquired LOCK...\n", tid);
        pthread_mutex_lock(&mutex);

        fprintf(stderr, "[PRODUCER-%ld] INSERTING item %d at index %d\n", tid, item, in);
        buffer[in] = item;
        in = (in + 1) % BufferSize;

        fprintf(stderr, "[PRODUCER-%ld] RELEASED LOCK...\n", tid);
        pthread_mutex_unlock(&mutex);

        fprintf(stderr, "[PRODUCER-%ld] SIGNALED FULL condition...\n", tid);
        sem_post(&full);
    }
}

void *consumer(void *arg)
{
    long tid = syscall(__NR_gettid);
    sleep(1);                                          // Sleep to illustrate timing differences clearly
    int item;
    for (int i = 0; i < MaxItems; ++i)
    {
        fprintf(stderr, "[CONSUMER-%ld] Going to WAIT for FULL condition...\n", tid);
        sem_wait(&full);

        fprintf(stderr, "[CONSUMER-%ld] Acquired LOCK...\n", tid);
        pthread_mutex_lock(&mutex);

        fprintf(stderr, "[CONSUMER-%ld] REMOVING item %d from index %d\n", tid, buffer[out], out);
        item = buffer[out];
        out = (out + 1) % BufferSize;

        fprintf(stderr, "[CONSUMER-%ld] RELEASED LOCK...\n", tid);
        pthread_mutex_unlock(&mutex);

        fprintf(stderr, "[CONSUMER-%ld] SIGNALED EMPTY condition...\n", tid);
        sem_post(&empty);
    }
}

int main()
{

    pthread_t pro[5], con[5];
    pthread_mutex_init(&mutex, NULL);
    sem_init(&empty, 0, BufferSize);
    sem_init(&full, 0, 0);

    for (int i = 0; i < 5; ++i)
        pthread_create(&pro[i], NULL, &producer, NULL);

    for (int i = 0; i < 5; ++i)
        pthread_create(&con[i], NULL, &consumer, NULL);

    for (int i = 0; i < 5; ++i)
        pthread_join(pro[i], NULL);

    for (int i = 0; i < 5; ++i)
        pthread_join(con[i], NULL);

    pthread_mutex_destroy(&mutex);
    sem_destroy(&empty);
    sem_destroy(&full);

    return 0;
}
