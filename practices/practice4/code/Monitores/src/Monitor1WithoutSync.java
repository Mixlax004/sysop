    class NoSyncTable{  
    
    void printTable(int n){          /// ejecuta primero sin la palabra synchronized, luego con ella y observa las diferencias 
       for(int i=1;i<=5;i++){  
         System.out.println(n*i);  
         try{  
          Thread.sleep(400);  
         }catch(Exception e){System.out.println(e);}  
       }  
      
     }  
    }  
      
    class NoSyncMyThread1 extends Thread{  
        NoSyncTable t;  
        NoSyncMyThread1(NoSyncTable t){  
        this.t=t;  
        }  
        public void run(){  
            t.printTable(5);  
    }  
      
    }  
    class NoSyncMyThread2 extends Thread{  
        NoSyncTable t;  
        NoSyncMyThread2(NoSyncTable t){  
            this.t=t;  
        }  
        public void run(){  
            t.printTable(100);  
        }  
    }  
      
    public class Monitor1WithoutSync{  
        public static void main(String args[]){  
            NoSyncTable obj = new NoSyncTable();//only one object  
            NoSyncMyThread1 t1=new NoSyncMyThread1(obj);  
            NoSyncMyThread2 t2=new NoSyncMyThread2(obj);  
            t1.start();  
            t2.start();  
        }  
    }  
    

