 class NoSyncMonitorExample {
    private int count = 0;

    public void increment() {
        while (count == 2) {
            System.out.println("Waiting for incrementing");
        }
        count++;
    }

    public void decrement() {
        while (count == 0) {
            System.out.println("Waiting for decrementing");
        }
        count--;
    }

    public int getCount() {
        return count;
    }
}

 class NoSyncIncrementer implements Runnable {
    private NoSyncMonitorExample monitor;

    public NoSyncIncrementer(NoSyncMonitorExample monitor) {
        this.monitor = monitor;
    }

    public void run() {
        try {
            while (true) {
                monitor.increment();
                monitor.increment();

                System.out.println("Contador incrementado: " + monitor.getCount());
                Thread.sleep(100);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

 class NoSyncDecrementer implements Runnable {
    private NoSyncMonitorExample monitor;

    public NoSyncDecrementer(NoSyncMonitorExample monitor) {
        this.monitor = monitor;
    }

    public void run() {
        try {
            while (true) {
                monitor.decrement();
                System.out.println("Contador decrementado: " + monitor.getCount());
                Thread.sleep(500);
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }
    }
}

public class Monitor2WithoutSync {
    public static void main(String[] args) {
        NoSyncMonitorExample monitor = new NoSyncMonitorExample();
        Thread incrementerThread = new Thread(new NoSyncIncrementer(monitor));
        Thread decrementerThread = new Thread(new NoSyncDecrementer(monitor));
        incrementerThread.start();
        decrementerThread.start();
    }

}
