import java.util.*;

public class interf {
    private static int[][] memoria;
    private static Queue<Proceso> colaProcesos;
    private static List<Proceso> procesosEliminados;
    private static List<Proceso> procesosFinalizados;

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Elige el tama\u00f1o de pagina \n1) 16 \n2) 32 \n3) 64");
        int tamanoPagina = scanner.nextInt();
        switch (tamanoPagina) {
            case 1:
                System.out.println("pagina tama\u00f1o 16");
                tamanoPagina = 16;
                inicializarMemoria(128);
                break;
            case 2:
                System.out.println("pagina tama\u00f1o 32");
                tamanoPagina = 32;
                inicializarMemoria(64);
                break;
            case 3:
                System.out.println("pagina tama\u00f1o 64");
                tamanoPagina = 64;
                inicializarMemoria(32);
                break;
            default:
                System.out.println("error anidado");
        }

        int opcion;
        do {
            int tamanoProceso = obtenerTamanoAleatorio();
            System.out.println("El tama\u00f1o asignado aleatoriamente es: " + tamanoProceso);
            System.out.println("1) Crear Proceso nuevo");
            System.out.println("2) Ver estado de los procesos");
            System.out.println("3) Ver estado de la memoria(localidades ocuadas por procesos)");
            System.out.println("4) Imprimir cola de procesos");
            System.out.println("5) Ver proceso actual");
            System.out.println("6) Ejecutar proceso actual");
            System.out.println("7) Pasar al proceso siguiente");
            System.out.println("8) Matar proceso actual");
            System.out.println("9) Desfragmentar memoria");
            System.out.println("10) Salir del programa");
            System.out.println("Elige una opci\u00f3n");
            opcion = scanner.nextInt();
            System.out.println("El numero ingresado es: " + opcion);
            switch (opcion) {
                case 1:

                    int tamanoMem = EspDis();
                    System.out.println("el numero de marcos disponibles es : " + tamanoMem);
                    int tamProsPaginado = tamanoProceso / tamanoPagina;
                    System.out.println("El numero de paginas del proceso es: " + tamProsPaginado);

                    if (tamProsPaginado <= tamanoMem) {

                        System.out.println("Escribe el nombre del proceso: ");
                        String nameProces = scanner.next();
                        Proceso proceso1 = new Proceso(nameProces, tamProsPaginado);
                        System.out.println(proceso1);
                        colaProcesos.add(proceso1);
                        //Proceso proceso5 = colaProcesos.peek();
                        //System.out.println("Proceso en la cola de procesos: " + String.valueOf(proceso5));

                        asignarValoresAMemoria(proceso1);

                    }
                    break;
                case 2:
                    verEstadoProcesos();
                    break;
                case 3:
                    verEstadoMemoria(tamanoPagina);
                    break;
                case 4:
                    imprimirColaProcesos();
                    break;
                case 5:
                    verProcesoActual(tamanoPagina);
                    break;
                case 6: // Ejecutar proceso actual
                    Proceso procesoEjecutar = colaProcesos.poll(); 
                    procesoEjecutar.ejecutarInstrucciones();
                    if (procesoEjecutar.getNumeroInstrucciones() == 0){
                        System.out.println("El proceso " + procesoEjecutar + " ha finalizado sus instrucciones");
                        procesosFinalizados.add(procesoEjecutar);

                    } else {
                        colaProcesos.add(procesoEjecutar);
                    }
                    break;
                case 7: // Pasar al proceso siguiente
                    colaProcesos.add(colaProcesos.poll());
                    break;
                case 8: // Matar proceso actual
                    Proceso procesoEliminar = colaProcesos.poll();
                    liberarMemoria(procesoEliminar);
                    System.out.println("El proceso " + procesoEliminar + " fue eliminado");
                    procesosEliminados.add(procesoEliminar);
                    break;
                case 9:
                    int ultimoRevisado = memoria.length;
                    for (int i=0; i<memoria.length; i++){
                        if (ultimoRevisado-i <= 0){
                            break;
                        }
                        if (memoria[i][0] == -1){
                            while (ultimoRevisado-i > 0){
                                ultimoRevisado--;
                                if (memoria[ultimoRevisado][0] != -1){
                                    memoria[i][0] = memoria[ultimoRevisado][0];
                                    memoria[i][1] = memoria[ultimoRevisado][1];
                                    memoria[ultimoRevisado][0] = -1;
                                    memoria[ultimoRevisado][1] = -1;
                                    break;
                                }
                            }
                        }

                    }

                    break;
                case 10:
                    System.exit(0);
                default:
                    System.out.println("opcion no valida");
            }
        } while (opcion != 10);

        scanner.close();
    }

    public static int obtenerTamanoAleatorio() {
        Random random = new Random();
        int[] tamanos = new int[] { 64, 128, 256, 512 };
        int indiceAleatorio = random.nextInt(tamanos.length);
        return tamanos[indiceAleatorio];
    }

    public static int EspDis() {
        int localidades = 0;

        for (int i = 0; i < memoria.length; i++) {
            if (memoria[i][0] == -1) {
                localidades++;
            }
        }

        return localidades;
    }

    public static List<Integer> idsLibre() {
        List<Integer> idslibre = new ArrayList<>();
    for (int i = 0; i < memoria.length; i++) {
        if (memoria[i][0] == -1) {
                idslibre.add(i);
            }
        }
        return idslibre;
    }

    public static void inicializarMemoria(int tamanoMemoria) {
        memoria = new int[tamanoMemoria][2];
        for (int i = 0; i < tamanoMemoria; i++) {
            memoria[i][0] = -1; //Proceso que lo ocupa, -1 si no hay ninguno
            memoria[i][1] = -1; //Página del proceso, -1 si no hay ninguno
        }
        colaProcesos = new LinkedList<Proceso>();
        procesosEliminados = new ArrayList<Proceso>();
        procesosFinalizados = new ArrayList<Proceso>();
    }
    
    public static void liberarMemoria(Proceso procesoLiberar) {
        for (int i=0; i<memoria.length; i++){
            if (memoria[i][0] == procesoLiberar.getId()){
                memoria[i][0] = -1;
            }
        }
    }


    public static void asignarValoresAMemoria(Proceso procesoAsignar) {
        List<Integer> idsLibres = idsLibre(); // Obtiene los IDs libres
        int pagsRestantes = procesoAsignar.getEspacioRequerido();
        int valorAsignado = 1; // Inicia el valor a asignar en 1

        for (Integer id : idsLibres) {
            if (pagsRestantes == 0){
                break;
            }
            memoria[id][0] = procesoAsignar.getId();
            memoria[id][1] = valorAsignado;
            pagsRestantes--;
            valorAsignado++;
            }
        }

    public static void imprimirColaProcesos() {
        System.out.println("Cola de Procesos:");
        if (colaProcesos.isEmpty()) {
            System.out.println("No hay procesos en la cola.");
        } else {
            for (Proceso proceso : colaProcesos) {
                System.out.println("ID: " + proceso.getId() + ", Nombre: " + proceso.getNombre() +
                                   ", Instrucciones Pendientes: " + proceso.getNumeroInstrucciones());
            }
        }
    }

    public static void imprimirProcesosFinalizados() {
        System.out.println("Procesos Finalizados:");
        if (procesosFinalizados.isEmpty()) {
            System.out.println("No hay procesos finalizados.");
        } else {
            for (Proceso proceso : procesosFinalizados) {
                System.out.println("ID: " + proceso.getId() + ", Nombre: " + proceso.getNombre() +
                                   ", Instrucciones Pendientes: " + proceso.getNumeroInstrucciones());
            }
        }
    }

    public static void imprimirProcesosEliminados() {
        System.out.println("Procesos Eliminados:");
        if (procesosEliminados.isEmpty()) {
            System.out.println("No hay procesos eliminados.");
        } else {
            for (Proceso proceso : procesosEliminados) {
                System.out.println("ID: " + proceso.getId() + ", Nombre: " + proceso.getNombre() +
                                   ", Instrucciones Pendientes: " + proceso.getNumeroInstrucciones());
            }
        }
    }

    public static void verEstadoProcesos() {
        imprimirColaProcesos();
        imprimirProcesosFinalizados();
        imprimirProcesosEliminados();
    }

    public static void verProcesoActual(int tamanoPagina) {
        Proceso procesoActual = colaProcesos.peek();
        System.out.println("Proceso Actual");
        System.out.println("ID: " + procesoActual.getId() + ", Nombre: " + procesoActual.getNombre() +
                                   ", Instrucciones Totales: " + procesoActual.getTotalInstrucciones() +
                                   ", Instrucciones Ejecutadas: " +
                                   (procesoActual.getTotalInstrucciones()-procesoActual.getNumeroInstrucciones())); 
        verDireccionesProceso(procesoActual, tamanoPagina);

    }

    public static void verDireccionesProceso(Proceso proceso, int tamanoPagina){
        System.out.println("Direcciones en memoria:");
        for (int i=0; i<memoria.length; i++){
            if (memoria[i][0] == proceso.getId()){
                System.out.println("Página " + memoria[i][1] + " en marco " + (i+1) + " con direcciones " + ((i*tamanoPagina)+1) + "..." + ((i+1)*tamanoPagina));
            }
        }
    }

    public static void verEstadoMemoria(int tamanoPagina){
        System.out.println("Estado de la memoria:");
        String nombreProceso = "error";
        for (int i=0; i<memoria.length; i++){
            if (memoria[i][0] == -1){
                System.out.println("Localidades: " + ((i*tamanoPagina)+1) + "..." + ((i+1)*tamanoPagina) + " libres");
            } else {
                for (Proceso proceso : colaProcesos){
                    if (proceso.getId() == memoria[i][0]){
                        nombreProceso = proceso.getNombre();
                        break;
                    }
                }
                System.out.println("Localidades: " + ((i*tamanoPagina)+1) + "..." + ((i+1)*tamanoPagina) +
                        " ocupadas por proceso con nombre: " + nombreProceso);
            }
        }

    }

}
