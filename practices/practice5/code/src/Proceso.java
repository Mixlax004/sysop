import java.util.List;
import java.util.ArrayList;

public class Proceso {
    private static int contadorID = 0; // Contador estático para asignar IDs únicos
    private int id;
    private String nombre;
    private int totalInstrucciones;
    private int numeroInstrucciones;
    private int espacioRequerido;

    // Constructor
    public Proceso(String nombre, int espacioRequerido) {
        this.id = ++contadorID; // Incrementar el contador y asignar ID único
        this.nombre = nombre;
        this.totalInstrucciones = generarNumeroAleatorio(10, 30); // Número aleatorio de instrucciones
        this.numeroInstrucciones = totalInstrucciones;
        this.espacioRequerido = espacioRequerido;
    }

    // Método para generar un número aleatorio entre min y max (inclusive)
    private int generarNumeroAleatorio(int min, int max) {
        return (int) (Math.random() * (max - min + 1)) + min;
    }

    // Getters
    public int getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public int getNumeroInstrucciones() {
        return numeroInstrucciones;
    }

    public int getTotalInstrucciones() {
        return totalInstrucciones;
    }

    public int getEspacioRequerido() {
        return espacioRequerido;
    }

    public void ejecutarInstrucciones() {
        numeroInstrucciones = numeroInstrucciones - 5;
        if (numeroInstrucciones < 0){
            numeroInstrucciones = 0;
        }
    }

    // Método para representar el proceso como una cadena de texto
    @Override
    public String toString() {
        return "Proceso{" +
                "id=" + id +
                ", nombre='" + nombre + '\'' +
                ", numeroInstrucciones=" + numeroInstrucciones +
                ", espacioRequerido=" + espacioRequerido +
                '}';
    }
}
