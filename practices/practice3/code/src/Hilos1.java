public class Hilos1 {
       
     public static void main(String[] args) {
         System.out.println("Inicio del programa (hilo) principal");
       
         MiHilo2 prueba1 = MiHilo2.crearYComenzar("th #1");
         MiHilo2 prueba2 = MiHilo2.crearYComenzar("th #2");
         MiHilo2 prueba3 = MiHilo2.crearYComenzar("th #3");
         System.out.println("Hilo principal finalizado");
     }
}
