// C program for implementation of FCFS  
// scheduling 
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<string.h>

#define MAXCHAR 1000
struct proc{ char procNum; int burstTime; int priority; int arrivalTime; };

static struct proc * getProcesses(){
    static struct proc process[5];
    FILE *fp;
    char row[MAXCHAR];
    char *token;

    fp = fopen("../processes.csv","r");

    ;

    for (int k = 0; k<6; k++)
    {
        fgets(row, MAXCHAR, fp);

        if (k>=1){
            token = strtok(row, ",");
            for (int i = 0; i<4; i++) {
                switch (i) {
                    case 0:
                        process[k-1].procNum = token[1];
                        break;
                    case 1:
                        process[k-1].burstTime = atoi(token);
                        break;
                    case 2:
                        process[k-1].priority = atoi(token);
                        break;
                    case 3:
                        process[k-1].arrivalTime = atoi(token);
                        break;
                }
                token = strtok(NULL, ",");
            }
        }
    }
    return process;
}

static struct proc * orderProcesses(struct proc * process, int choice){
    struct proc auxProx;
    switch (choice) {
        case 1:
            for (int i = 0; i<4; i++) {
                for (int j = 0; j<4; j++) {
                    if (process[j].arrivalTime > process[j+1].arrivalTime) {
                        auxProx = process[j];
                        process[j] = process[j+1];
                        process[j+1] = auxProx;
                    }
                }
            }
            break;
        case 2:
            for (int i = 0; i<4; i++) {
                for (int j = 0; j<4; j++) {
                    if (process[j].burstTime > process[j+1].burstTime) {
                        auxProx = process[j];
                        process[j] = process[j+1];
                        process[j+1] = auxProx;
                    }
                }
            }
            break;
        case 3:
            for (int i = 0; i<4; i++) {
                for (int j = 0; j<4; j++) {
                    if (process[j].priority > process[j+1].priority) {
                        auxProx = process[j];
                        process[j] = process[j+1];
                        process[j+1] = auxProx;
                    }
                }
            }
            break;
    }
    return process;
}

int main(){
    int choice;
    struct proc * process = getProcesses();
    printf("Welcome to process planning algoritms, please select your option according to the algorithm you want:\n1.First Come\n2.Shortest Job\n3.Priority\nYour selection: ");
    scanf("%d", &choice);
    process = orderProcesses(process, choice);
    switch (choice) { 
        case 1:
            printf("Process order by First Come algorithm:\n");
            break;
        case 2:
            printf("Process order by Shortest Job algorithm:\n");
            break;
        case 3:
            printf("Process order by Priority algorithm:\n");
            break;
    }
    for (int i = 0; i<5; i++) {
        printf("Process p%c\n",process[i].procNum);
    }
}
