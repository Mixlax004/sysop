public class ProductorConsumidorM {

    public static void main(String[] args) {
        BufferM buff = new BufferM();
        ProducerM p1 = new ProducerM(buff, 1);
        ConsumerM c1 = new ConsumerM(buff, 1);
        p1.start(); 
        c1.start();
    }
    
}
