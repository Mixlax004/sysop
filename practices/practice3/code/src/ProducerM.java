class ProducerM extends Thread{
    BufferM buff;
    int number;

    public ProducerM(BufferM buff, int number) {
      this.buff = buff;
      this.number = number;
    }

    @Override
    public void run() {
        int[] pack = new int[50];
        int data;
        for (int i = 0; i < 10; i++) {
            for (int k = 0; k < 50; k++){
                data = (int) (Math.random() * 100);
                pack[k] = data;
            }
        buff.put(pack);
        System.out.println("Productor #" + this.number + " put pack with 1st value: " + pack[0]);
        try {
            sleep((int)(Math.random() * 100));
        } catch (InterruptedException e) { }
        }
    }
}
