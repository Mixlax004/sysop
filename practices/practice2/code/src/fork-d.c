#include <stdio.h>
#include <unistd.h>

void imprimirLetras(char laLetra){
    int i, j, basura;

    for (i=1;i<1000;i++){
        printf("%c",laLetra);
        if ((i%60)==0) printf("\n");
        basura = 0;
        for (j=0; j<10000; j++) basura++;
    }
    printf("\n");
}

int main() {
    int pid;
    pid = fork();
    if (pid==0){
        imprimirLetras('H');
    } else {
        imprimirLetras('P');
    }
    return 0;
}
