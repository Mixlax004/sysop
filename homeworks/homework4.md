1. Investiga qué otras técnicas hay para la administración de memoria disponible además de la lista ligada de huecos y el mapa de bits.

Existen al menos dos técnicas adicionales:

- Agrupación: Consiste en almacenar las direcciones de los bloques libres en el primer bloque libre. El primer bloque libre almacena las direcciones de *n* bloques, para los bloques que quedan fuera de este conjunto se asigna el primer bloque fuera de los *n* bloques para que almacene las
  direcciones de otros *n* bloques y así sucesivamente. Una ventaja de esta técnica es que las direcciones de un grupo de bloques libres pueden ser encontradas facilmente.
  
- Conteo: Este enfoque almacena la dirección del primer bloque libre y un número de *n* bloques libres contiguos al primer bloque. Cada entrada en esta lista contiene una dirección del primer bloque libre y un número *n*.

2. ¿Qué es la paginación en 2 niveles?

Para realizar la paginación debemos dividir nuestros procesos y nuestra memoria en páginas. Para mantener el control de qué marcos (páginas físicas) almacenan qué páginas (páginas lógicas) se ha de mantener una *Tabla de Páginas* que registre estas relaciones para cada proceso. La tabla de páginas se almacena a su
vez en un espacio de la memoria, si el tamaño de esta tabla es menor que el tamaño del marco entonces se puede acceder a ella directamente, sin embargo, si el tamaño de esta tabla es mayor que el tamaño de marco entonces esta tabla también se ha de dividir en páginas y por lo tanto se ha de crear
otra tabla que relacione los contenidos de la primera con los marcos donde se almacena.

A la nueva tabla se le llama *Tabla de Paginación Externa*, mientras que a la original se le llama *Tabla de Páginación Interna*. A este mecanismo se le conoce como paginación en dos niveles

3. ¿Qué es una tabla de página invertida?

Se trata de una estructura de datos diseñada para mapear páginas físicas de memoria (frames) con páginas lógicas, en este caso, en lugar de existir una tabla para cada proceso que relaciona las páginas lógicas con las físicas, existe una tabla para cada frame que relaciona dicho frame con la página
lógica.

Una de las principales ventajas de este modelo es que solo se requiere una porción **fija** de memoria para almacenar la paginacion de todos los procesos.

4. Investiga cómo se implementa la paginación en Linux y en Windows explicando cada uno

Según Tanenbaum, así se describe la paginación en estos dos sistemas operativos:

- Linux:

Aquí solo se requiere que la estructura del usuario de un proceso y la propia tabla del proceso estén cargadas en memoria para que el proceso se considere "en memoria", las páginas del texto, datos y pila se cargan dinámicamente uno a la vez. Si la estructura de usuario y la tabla del proceso no
están en memoria entonces el proceso no puede ejecutarse.

La paginación es implementada en parte por el kernel y en parte por un nuevo proceso llamado *page daemon* que se ejecuta periódicamente.

Linux es un sistema que funciona completamente en paginación bajo demanda sin pre-paginación.

- Windows:

Windows es un sistema de paginación bajo demanda, aquí no se soporta la segmentación. La paginación bajo demanda en el administrador de memoria del sistema operativo es controlado por las *page faults*. En cada *page fault* ocurre una *trap* al kernel y el CPU entra en modo kernel. El kernel
contrstruye un descriptor *machine-independent* explicando lo que sucedió y pasa esto a la parte del administrador de memoria. Entonces el administrador de memoria revisa el acceso por validez. Si la página *faulted* cae en la región comentada y el acceso está permitido, revisa la dirección en el
árbol VAD (Virtual Access Descriptor) y encuentra (o crea) la entrada de la tabla de páginas del proceso.

Generalmente la memoria paginable se divide en *páginas privadas* y *páginas compartibles*. Las páginas privadas solo tienen significado dentro del proceso que las posee; no son compartibles con otros procesos. De manera que estas páginas siempre se liberan cuando el proceso termina su ejecución.

En general, existen muchos más detalles referentes a la páginación de estos sistemas, aquí solo se describen breves características de la superficie de la técnica.
