1. Explica qué es un contenedor, cómo funciona y cuáles son sus principales diferencias con
respecto a una máquina virtual.

En un contexto informático, los contenedores representan un tipo de *virtualización* cuyo objetivo suele estar relacionado con aislar conjuntos de software entre sı́ en una misma
máquina fı́sica. Los contenedores se diferencian de las máquinas virtuales (VMs) en la medida en que estas últimas reservan un *pool* de recursos del sistema aunque estos no representen
en realidad la cantidad más óptima para cumplir los objetivos para los que fueron creadas.

Los contenedores, por otra parte, poseen la caracterı́stica de que comparten el mismo kernel (gracias a las caracterı́sticas de Linux para lidiar con múltiples usuarios simultaneamente)
y distribuyen mucho mejor los recursos que requieren para ejecutarse. Los contenedores encuentran sus aplicaciones en diferentes campos de la industria TI, como lo son las aplicaciones
basadas en microservicios, CI/CD, escalabilidad horizontal, entre otros.

En conclusión, los contenedores como teconolgı́a reciente constituyen una reciente revolución en el despliegue de aplicaciones en entornos profesionales, pero si bien marcan un
estándar en entornos modernos no son la única ni la tecnologı́a más apropiada para todos los casos de la industria.

Referencias:
- https://cloud.google.com/learn/what-are-containers?hl=es
- https://www.ibm.com/es-es/topics/containers
- https://aws.amazon.com/es/compare/the-difference-between-containers-and-virtual-machines/
2. Investiga y explica brevemente la historia de los intérpretes de comandos de Linux (Shell) y
realiza una tabla comparativa indicando las principales diferencias entre 3 de ellas.

El término Shell fue acuñado por Louis Pouzin concibió la idea de utilizar comandos de alguna manera como un lenguaje de programación, utilizó esta palabra para describirlo. El sistema fue implementado para Multilics por Glenda Schroeder y un autor desconocido de General Electric.

Ken Thompson implementó la *Thompson Shell* en 1971 en la primera versión de UNIX agregando características novedosas. Mientras que la primera *Shell* gráfica apareció en el sistema NLS en 1968.

Más allá de la Shell de Thompson, los intérpretes modernos se empiezan a ver desde 1977 cuando apareció la *Bourne Shell*, sus objetivos eran servir como un intérprete para ejecutar comandos de forma interactiva y para hacer *scripting* (Escribir conjuntos de comandos reutilizables que puedieran ser
invocados mediante el intérprete), introdujo controles de flujo, ciclos y variables dentro de los *scripts* así como también la sustitución de comandos.

La *Bourne-Again Shell* o Bash es un proyecto de software libre de GNU que pretendía reemplazar a la *Bourne Shell*. Bash se ha convertido en uno de los intérpretes más famosos apareciendo en distribuciónes GNU/Linux, Farwin, Windows, Cygwin, Novell, Haiku y más. Esta permite ejecutar la mayoría de
los comandos que existían en *Bourne Shell*. Bash siguió evolucionando, permitiendo expresiones regulares, y *associative arrays*.

Con base en *Bash* y en otras se han creado diferentes *Shells* novedosas y exóticas, algunas de ellas son:
- Scheme Shell
- Pyshell
- BusyBox
- Fish

| Bourne                           | Bash                                                                            | Fish                                         |
|----------------------------------|---------------------------------------------------------------------------------|----------------------------------------------|
| Introduce scripting              | Posee expresiones regulares                                                     | Sugiere comandos con base en el historial    |
| Añade la sustitución de comandos | Creció enormemente en popularidad y fue utilizada por diferentes sistemas       | Soporta colores en 24 bits                   |
| Incluye ciclos y variables       | Su licencia de uso y filosofía es libre; respeta las libertades de los usuarios | Su scripting es simple, limpio y consistente |
3. Explica qué versión de Shell es la que se encuentra en la distribución que instalaste e
investiga como se realiza un script “ejecutable”.

Por defecto, Arch Linux incluye *Bash* como su shell, sin embargo en mi caso específico decidí cambiar la shell a *Fish*. La creación de ejecutables en *Bash* se realiza en archivos *.sh* (en la primera línea del script se especifica la ruta del intérprete que lo ha de ejecutar) comunmente y tiene en consideración los comandos del intérprete, la redirección de entrada y salida, las
tuberías, las variables, la sustitución de comandos, las expresiones regulares, entre otros. En *Fish* el caso es similar, con algunas diferencias en comandos; la diferencia más notoria para mí reside en la asignación de valores a las variables, en *Bash* se pueden asignar mediante el símbolo de
igualdad, mientras que en *Fish* se ha de utilizar el comando "export" o "set".
4. Para cada una de las siguientes categorías investiga al menos 3 llamadas al sistema, su
versión en Linux y en Windows y explica brevemente para que sirven:

- Control de procesos:
    - Creación de procesos: Windows CreateProcess(), Linux fork().
    - Terminación de proceso: Windows ExitProcess(), Linux exit().
    - Obtener el process ID: Windows GetCurrentProcessId(), Linux getpid().
- Administración de archivos:
    - Abrir un archivo o dispositivo: Windows CreateFile(), Linux open().
    - Cerrar un archivo o dispositivo: Windows CloseHandle(), Linux close().
    - Leer de un archivo o dispositivo: Windows ReadFile(), Linux read().
- Control de dispositivos:
    - Mapear archivos o dispositivos en memoria: Windows MapViewOfFile(), Linux mmap().
    - Aumentar el espacio reservado para un programa: Windows VirtualAlloc(), Linux brk().
    - Liberar espacio reservado para un programa: Windows VirtualFree(), Linux sbrk().
- Integridad:
    - Cambiar el propietario de un archivo: Windows SetSecurityInfo(), Linux chown().
    - Cambiar el grupo de un archivo: Windows SetSecurityInfo(), Linux chgrp().
    - Cambiar los permisos de un archivo: Windows SetFileAttributes(), Linux chmod().
- Comunicaciones:
    - Crear un nuevo socket: Windows socket(), Linux socket().
    - Escuchar por conexiones en un socket: Windows listen(), Linux listen().
    - Aceptar una nueva conexión en un socket: Windows accept(), Linux accept().
5. Leer el texto Ontogenia y Filogenia y hacer un comentario o una conclusión al respecto del
tema, indicar algún ejemplo donde se observe esta situación.

Desde la perspectiva de la computación las características comunes (debido a que aquellos que las presentan provienen de un mismo "ancestro") es bastante frecuente, esto se puede evidenciar en situaciones como los sistemas GNU/Linux, los intérpretes de comandos, entre otras. Podríamos decir que las
versiones más recientes "evolucionan" con respecto a sus antecesoras pero conservan algunas de sus cualidades.
