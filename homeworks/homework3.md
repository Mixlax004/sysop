1. Investiga qué es y en qué consiste la planificación por colas multinivel retroalimentadas

Este algoritmo de planificación es muy similar a las colas multinivel (sin retroalimentación) con la diferencia de que el algoritmo aquí descrito además de separar los procesos en múltiples colas de procesos listos, los procesos pueden moverse entre las colas con base en algunas de sus
características; se les da prioridad a aquellos que tengan cortas ráfagas de CPU y grandes ráfagas de procesos de entrada y salida.

En cierto sentido las colas multinivel retroalimentadas podrían describirse como un híbrido entre las colas multinivel y el algoritmo Shortest Job First (SJF).

El algoritmo de planificación de las colas multinivel retroalimentadas fue desarrollado originalmente por Fernando J. Corbató en 1962 (An experimental time-sharing system), por su logro se le reconoció con el premio Turing.
2. Investiga y cómo se realiza en forma general la planificación de hilos

Según Tanenbaum, la planificación de hilos puede categorizarse dependiendo de si el sistema soporta hilos a nivel usuario o hilos a nivel kernel (o ambos).

En el caso de los hilos a nivel usuario, como el kernel no es consciente de la existencia de los hilos, selecciona e interrumpe los procesos de la forma que lo hace habitualmente. Asigna para cada proceso el tiempo que considera apropiado sin importar lo que ocurra dentro de los procesos a nivel del
planificador de hilos de cada uno.

Con los hilos a nivel kernel, este selecciona un hilo específico para ejecutarlo. No necesita conocer el proceso al que cada hilo pertenece pero puede hacerlo si le hace falta. Cada hilo recibe un intervalo de tiempo para acceder al procesador y se interrumpe si se excede.

Los hilos a nivel usuario y a nivel kernel presentan algunas diferencias; para la realización de conmutación de hilos, el nivel usuario es considerablemente más rápido, pero por otra parte, en el nivel de kernel cuando un hilo se bloquea esperando una señal de E/S no se interrumpe todo el proceso.
3. Investiga en qué consisten los conceptos de Multithreading o Hyperthreading y las principales diferencias entre ellos.

En el libro de Tanenbaum se habla sobre este tema indicando que Hyperthreading solo es el nombre que le asignó Intel al Multithreading

El multithreading refiere a la capacidad del CPU de contener el estado de dos hilos de ejecución distintos alternando entre uno y otro con una escala de tiempo minúscula (de nanosegundos). El autor señala que el multithreading no es un verdadero paralelismo, solo una alternación tan rápida que da la
impresión de que lo fuera.

Realizando una comparativa con los conceptos ya vistos, el multithreading se podría describir como un multiprocesamiento pero a nivel de hilos.

4. Leer el documento, Ejemplos de planificación en sistemas operativos y realiza un breve resumen de la forma en que se realiza en Solaris, Windows y Linux
