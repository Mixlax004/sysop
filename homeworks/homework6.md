1. Explica como se realiza el proceso de montaje de sistemas de archivos.

El montaje es un proceso por el cual un sistema operativo hace que los archivos de un dispositivo de almacenamiento (como un disco duro, CD-ROM, etc) disponible para que los usuarios lo accedan a través del sistema de archivos. (Indiana University-University Information Technology Services. 18 de febrero de 2011).

En general, en el proceso de montaje el sistema operativo adquiere acceso al dispositivo de almacenamiento; reconociendo, leyendo y procesando la estructura del sistema de archivos y los metadatos en él para que pueda ser accedido por el usuario.

2. Investiga qué son y las principales diferencias entre UEFI y BIOS así como los tipos de particiones que implementa cada uno.

Según Tanenbaum, en las tarjetas madre existe un pequeño programa que es el firmware del sistema. Este firmware, en sus inicios, se trataba del BIOS (Basic Input Output System), este sistema tenía sus desventajas; originalmente arrancar el computador con BIOS era lento, dependiente de la arquitectura
y limitado a SSDs pequeños y discos. Pero era fácil de entender. 

Más adelante, Intel propuso como reemplazo al UEFI (Unified Extensible Firmware Interface) que remediaba estos inconvenientes; permitía un arranque rápido, diferentes arquitecturas y tamaños de almacenamiento de hasta 2^70 bytes. Pero es mucho más complejo.

En BIOS se implementa el MBR (Master Boot Record) que es el primer sector de un dispositivo que esté diseñado para arrancarse a través de BIOS. Este contiene un programa que normalmente examina la tabla de particiones al final del sector de arranque para determinar que partición se encuentra activa
para leer un gestor de arranque que inicie esa partición. Este cargador lee el sistema operativo de la partición activa y lo inicia.

El UEFI no usa MBR, si no que revisa la ubicación de la tabla de particiones en el segundo sector del dispositivo desde el que se va a arrancar el sistema. Esta GPT (GUID Partition Table) contiene información acerca de la ubicación de varias particiones en el SSD o en el disco.

Por otra parte, el BIOS tiene la capacidad por si mismo para leer sistemas de archivos de tipos específicos, mientras que según el estándar UEFI, este debe por lo menos soportar los tipos FAT-12, FAT-16 y FAT-32. Uno de estos sistemas de archivos se coloca en una partición especial conocida como el
sistema de partición EFI (ESP), ya que en lugar de un único sector mágico de arranque, el proceso de arranque ahora puede usar un sistema de archivos apropiados que contienen programas, archivos de configuración y cualquier otra cosa que pueda ser útil durante el arranque.

De manera que, en UEFI se espera que el firmware pueda ejecutar programas en un cierto formato llamado PE (Portable Executable).

3. Investiga qué son y cómo funcionan los modelos RAID.

Según Tanenbaum, en 1988 Patterson et al. pulicaron un artículo que sugería seís configuraciones/disposiciones de un conjunto de discos que podrían ser utilizados para mejorar el rendimiendo, la consistencia, o ambas. Paterson et al. lo definieron como Redundant Array of Inexpensive Disk, pero la
industria cambió el Inexpensive por Independent.

La idea consiste en instalar una caja llena de discos al lado del computador. Aquí se ha de reemplazar el controlador de disco con un controlador de RAID. De modo que para los sistemas operativos un RAID se "ve" como un SLED(Single Large Expensive Disk) pero con mejor rendimiento y
mejor consistencia. Patterson et al. definieron diferentes esquemas de organización de estos RAID.

Para el RAID 0 se ve al único disco simulado por el RAID como uno dividido en "strips" de k sectores cada uno. El tema está en que estos strips se dividen uno por uno en cada disco existente, por ejemplo, si hay tres discos y seis strips, el strip 1 estará en el primer disco, el strip 2 en el segundo y
el tercer strip en el tercero, luego el cuarto estará en el primer disco, el quinto en el segundo y el sexto en el tercero. De manera que si el sistema operativo ordena leer un bloque de datos contenido en tres strips consecutivos, entonces cada disco será capaz de leer su strip por separado y en
paralelo.

El RAID 1 es un RAID "verdadero", ya que duplica todos los discos, de manera que para nuestro ejemplo se necesitarían de tres discos primarios y tres discos de backup. Cuando se escribe, cada strip se escribe dos veces. Cuando se lee, cada copia puede ser utilizada distribuyendo la carga sobre más
dispositivos. Entonces, la eficiencia de escritura no cambia, mientras que la de lectura puede incluso mejorar el doble. Tiene una excelente tolerancia a fallos ya que si falla un dispositivo se usa su backup.

El RAID 5 también usa strips pero con paridad distribuida, es decir, se dedica un strip en cada disco para almacenar la información de la paridad, de manera que se requiere que todos los discos menos uno estén presentes para poder operar.

Para el RAID 10 (o RAID 1+0) se utiliza el concepto de un "stripe" de "mirrors", ya que cada elemento del RAID 0 contiene a su vez un RAID 1 que posee como mínimo dos discos ya que uno es el duplicado del otro. De esta manera el conjunto puede soportar múltiples pérdidas de unidades siempre que
ningún espejo pierda todas sus unidades.

El RAID 01 (o RAID 0+1) es muy parecido al anterior, ya que aquí se crea un "mirror" de los "stripes", es decir, cada elemento del RAID 1 contiene a su vez un RAID 0, por cada RAID 0 que exista deberá haber otro RAID 0 que contenga los duplicados del primero. Aquí el riesgo durante la reconstrucción es significativamente mayor que en el RAID 1+0, ya que hay que leer todos los datos de todas las unidades del stripe restante en lugar de sólo los de una unidad.
