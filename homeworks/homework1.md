1. Investiga cómo es el proceso de arranque de una computadora ¿En qué punto de este
proceso entra en acción el sistema operativo?
En sistemas con unidades de procesamiento tipo x86, esta CPU inicia el proceso de arranque ejecutando en "real mode" la instrucción localizada en el "reset vector" (Una dirección de memoria específica) que suele apuntar al punto de entrada del firmware (UEFI o BIOS) en la ROM. Este programa ejecuta un "power on self test" (POST) para comprobar e inicializar los dispositivos requeridos, como lo puede ser la memoria principal. (Osborne, Adam; Kane, Gerry (1981))

Después de inicializar el hardware requerido, el firmware intenta acceder a una serie de dispositivos de almacenamiento no volátiles (en un orden pre-configurado) hasta encontrar uno que sea inicializable. Una vez lo encuentra, el firmware carga el sector de arranque de ese dispositivo y ejecuta el
código que se encuentre dentro de este, el cual se conoce como "Volume Boot Record" (VBR).

No existe un VBR estándar ya que varía con cada sistema operativo, pero comunmente su función suele residir en cargar y ejecutar el archivo de cargador de arranque para que este último cargue la imagen del kernel del sistema operativo. (Documentación de producto. Red Hat 2018)

En sistemas con kernel Linux, la imagen del sistema operativo se
descomprime después de haberse cargado, poseriormente se inicializan dispositivos, se monta el sistema de ficheros raiz y el ejecuta un proceso de inicialización (/sbin/init). El proceso "init" es un demonio que inicializa nuevos procesos, monta sistemas de ficheros y su función es iniciar el "espacio de usuario" (M. Tim
Jones 2006). "init" es el primer proceso en arrancarse en el sistema durante el encendido y el último en terminarse durante el apagado.

En respuesta a la pregunta en cuestión, podríamos considerar que el sistema operativo entra en juego en el momento inmediatamente posterior al que se carga la imagen del kernel, ya que a partir de este punto se empiezan a ejecutar instrucciones propiamente del kernel para terminar de inicializar
todo el sistema operativo.
2. Leer la “Historia de los sistemas operativos” del libro de Andrew Tanenbaum y hacer un
resumen de los principales puntos de cada etapa
En el libro de Tanenbaum se exponen cuatro generaciones en la historia de las computadoras:

---

En la primera generación se habla sobre el diseño y uso de las primeras computadoras existentes, las cuales eran creadas mediante tubos de vacío y su programación solía consistir en un tablero de conexiones que tenía que ajustarse manualmente para cada programa que se deseaba ejecutar. También se
habla sobre que aunque Charles Babagge hubiera diseñado una computadora mecánica en las primeras décadas del sigo XIX, su invención no se materializó en su momento debido a que se trataba de una propuesta muy ambiciosa en relación con las limitaciones tecnológicas de la época. Por otra parte, las
primeras computadoras fabricadas se diseñaron y construyeron debido a la presión de los gobiernos (Británicos y Estadounidenses) para conseguir ventajas estratégicas en el marco de la segunda guerra mundial. 

Las primeras computadoras eran bastante inconvenientes, costosas y solo se utilizaban para realizar cálculos numéricos simples. Aún así a principios de la década de los 50's se introdujeron las tarjetas perforadas para facilitar la programación de estas máquinas.

---

La segunda generación se vio marcada por la aparición del transistor, este nuevo artefacto tecnológico suponía una gran disminución en los costos de fabricación de las computadoras. Aún así, requerían de ser operadas por personal especializado en cuartos con aire acondicionado. En esta época
apareció el lenguaje de programación FORTRAN, en el cual se escribian los trabajos (programas o conjunto de programas) que la máquina compilaría y ejecutaría.

Debido a las inconveniencias de diseño, se ideó un sistema de procesamiento por lotes en el cual se recopilaban varias tarjetas (las cuales contenían los programas) que se pasarían por un lector de tarjetas (una computadora mucho más simple especializada en esa función) el cuál copiaría la
información de las tarjetas en una cinta magnética, esta cinta magnética luego le sería suministrada a la computadora que sí realizaría las operaciones para que entregrara el resultado en otra cinta magnética cuyo contenido tendría que ser impreso posteriormente.

El uso de las computadoras de la segunda generación se centraba en campos de la ciencia e ingeniería como la solución de ecuaciones diferenciales parciales.

---

La tercera generación tuvo un cambio similar a la segunda debido a la implementación de una novedad tecnológica en la fabricación de las computadoras, en este caso de trató de los circuitos integrados primeramente introducidos en la línea de computadoras IBM, empresa que a su vez innovó con una
estrategia empresarial que les permitió crear una sola línea de máquinas, con software compatible entre ellas, que cubrían las necesidades desus distintos tipos de clientes.

Por otra parte, la tercerá generación fue también marcada por la aparición del concepto de la multiprogramación, mediante este los trabajos en espera de una señal de entrada o salida podían dar paso a que otro trabajo utilizara la CPU para disminuir el tiempo de inactividad de la misma y aumentar la
eficiencia en general.

En esta generación aparecieron sistemas basados en la idea del "tiempo compartido" que consistía en que varios usuarios pudieran acceder a los mismos recursos de cómputo "simultaneamente". Gracias a este concepto se desarrollaron sistemas como CTSS, MULTILICS y el sistema de electricidad moderno.
Estos avances permitieron el desarrollo de nuevas ideas en el campo, como lo fue el caso de UNIX, un sistema operativo inspirado en MULTILICS(aunque más simple) desarrollado por uno de los científicos que trabajó en este último, su nombre era Ken Thompson.

Debido a que el código de UNIX era ampliamente disponible, algunas empresas crearon sus propias versiones; nacieron System V, BSD y se creó el estándar POSIX cuyo fin era que los programas pudieran ejecutarse en cualquier sistema UNIX. Más adelante Thompson creó una versión educativa de UNIX
conocida como MINIX, sistema operativo el cual sirvió de inspiración para Linus Torvalds para crear LINUX.

---

De nuevo, con la aparición de una novedad tecnológica, los circuitos de integración a gran escala, se redujeron enormemente los precios de fabricación y las computadoras se hicieron accesibles a las personas del común.

En esta generación, Bill Gates compró DOS y vendió el paquete junto con BASIC para que IBM pudiera instalarlo en sus máquinas, de esta manera Microsoft se popularizó como empresa al igual que sus posteriores sistemas operativos. Por otra parte, Steve Jobs introdujo el concepto (ya creado previamente por Doug Engelvart) de una interfaz gráfica en el sistema operativo de sus dispositivos Apple, windows por su parte también adoptó la idea del GUI y continuo creando nuevos sistemas operativos.

Por otra parte, los sistemas UNIX no se quedaron rezagados y se popularizaron en entornos de servidores, Linux se hizo popular entre estudiantes y entornos corporativos. También se creó una versión libre de BSD llamada FreeBSD la cuál fue posteriormente modificada y usada en las computadoras
Macintosh. En los sistemas UNIX se desarrolló un sistema de ventanas llamado X Window System que fue empleado por GUI's completas como Gnome o KDE.

Por último se empezaron a desarrollar y explorar los conceptos de sistemas operativos en red y sistemas operativos distribuidos, que perimitian a varias computadoras conectarse para compartir archivos y comunicarse para diferentes tareas.

3. Complementar la etapa histórica investigando acerca de los sistemas operativos móviles.

En años más recientes, con la aparición de los dispositivos móviles (smartphones y otros "smart devices") se han diseñado nuevos sistemas operativos específicos para estos tipos de computadores. Los sistemas operativos móviles suelen dividirse en dos sub-sistemas operativos ; uno para manejar la
capa de usuario y otro que suele ser completamente propietario para controlar las comunicaciones mediante SIM y otros componentes de hardware. (Holwerda, Thom. 2013)

Desde el año 2017 los sitemas operativos móviles comprenden la mayoría del mercado y los "smartphones" pasaron a ser los más utilizados por encima de cualquier otra clase de dispositivo. (TechFoogle. 2019)

5. Para cada uno de los siguientes tipos de sistemas operativos, investigar 2 ejemplos y
describirlos brevemente

- Mainframe

z/OS: Se trata de un sistema operativo de 64 bits escrito para los Mainframe de IBM con z/Architecture. Una de sus características más interesantes es que posee un balanceador de carga que maneja de forma automática múltiples unidades de trabajo de forma simultanea (Workload License Charges. IBM.
Tomado el 4 de febrero de 2024).

Red Hat Enterprise Linux (RHEL): Esta es una distribución GNU/Linux comercial con principal enfoque a servidores pero que también ha sido utilizada en mainframes. Recientemente (2023) esta versión comercial ha restringido el acceso a su código fuente (Proven, Liam. 2023).
- Servidores

Debian: Se trata de un sistema operativo GNU/Linux matendio completamente por su comunidad, se caracteriza por proveer software muy bien probado, por ello se le atribuye una gran estabilidad.

SUSE Linux Enterprise Server: Esta es otra distribución GNU/Linux creada por la compañía SUSE. Una de las características de este producto de fin comercial es el soporte que la empresa brinda a quienes lo adquieren.
- Embebidos

AsteriodOS: Se trata de una distribución GNU/Linux creada para reloges inteligentes. Este sistema operativo es mantenido por la comunidad y sus beneficios residen en brindar una experiencia de libertad y privacidad a sus usuarios, también soportan varios dispositivos de diferentes fabricantes

OpenWrt: Esta es otra distribución GNU/Linux diseñada para sistemas embebidos cuya función se enrutar tráfico de red. Soporta muchos dispositivos de diferentes fabricantes
- De tiempo real

FreeRTOS: Se trata de un Kernel de sistema operativo de tiempo real mantenido por el equipo de Amazon Web Services (AWS) y se caracteriza por su rápida ejecución de tareas.

PX5 RTOS: Este es un sistema operativo de tiempo real lanzado en 2023 diseñado para dispositivos embebidos. Una de sus características más notorias es su soporte para hilos POSIX, el cual es ausente en varios otros sistemas operativos homólogos.
- De dispositivos móviles

LineageOS: Se trata de un sistema operativo basado en Android con la mayoría de su código abierto o libre. Este sistema operativo es una de las alternativas más populares para aquellos que no desean usar los componentes propietarios que suelen acompañar a las distribuciones de Android (como las
Google Apps y otras aplicaciones de los fabricantes)

Ubuntu Touch: Se trata de una versión móvil del sistema operativo Ubuntu (Una distribuión GNU/Linux muy popular). Se trata de una alternativa a los sistemas operativos más populares en el entorno móvil (IOS y Android).
